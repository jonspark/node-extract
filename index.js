const transcodeHandler = require('./src/transcode');

const main = async () => {
  const s3Keys = [
    'some/path/to/input-file-1.mkv',
    'some/path/to/input-file-2.mkv',
    'some/path/to/input-file-3.mkv',
  ];
  const outputFile = 'an-example-output-file.webm';
  const composedFile = await transcodeHandler.createComposition(s3Keys, outputFile);

  return transcodeHandler.createJob(composedFile);
}

main().then(console.log)
