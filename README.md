## Node Developer Code Review

Here we have some code from an older client project's background processor. It forms part of a worker task that processes some raw audio files from a video call.

The service we used (Twilio), would record audio and video from each source as separate files. They offered a "composition" service, but it would take over 30 minutes for us to get the combined audio. Our client wanted a transcription of the audio to be available within 5 minutes of the call ending.

To do this, we had to bypass the composition service and combine the files ourselves. There weren't any web services at the time that would do this for us, so we had to write something.

The worker uses a command-line call to ffmpeg to generate the combined file and then sends it to AWS for further processing.
