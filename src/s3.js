const stream = require('stream')

const AWS = require('aws-sdk')

exports.uploadStream = fileName => {
  const { AWS_S3_BUCKET } = process.env
  const s3 = new AWS.S3({
    signatureVersion: 'v4',
  })

  const passThroughStream = new stream.PassThrough()

  return {
    writeStream: passThroughStream,
    promise: s3.upload({
      Bucket: AWS_S3_BUCKET,
      Region: 'eu-west-1',
      Key: fileName,
      Body: passThroughStream,
    }).promise(),
  }
}
