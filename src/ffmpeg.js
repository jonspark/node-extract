const { spawn } = require('child_process')

const spawnProcess = (command, args, options = {}) => new Promise((resolve, reject) => {
  const output = []
  const cmd = spawn(command, args, options)

  cmd.stdout.on('data', data => {
    console.log('stdout: ', data.toString())
    output.push(data.toString())
  })

  cmd.stderr.on('data', data => {
    console.log('stderr: ', data.toString())
  })

  cmd.on('close', code => {
    console.log('child process exited with code: ', code)
    if (code === 0) {
      resolve(output.join(''))
    } else {
      reject(output.join(''))
    }
  })
})

module.exports = {
  combineAudioFiles: (sources, output) => {
    // The first source is the earliest, we'll remove the start time from the others
    const delayDelta = sources[0].start

    // Builds the "[a0] [a1] ...[ax]" part of the mix arg
    const mix = sources.map((_source, index) => `[a${index}]`).join(' ')

    const resamples = [
      // The first source doesn't need any offsetting, just resample it
      '[0]aresample=async=1[a0];',

      // Map all the other sources and set up their offsets
      ...sources.slice(1).map((audioFile, index) => {
        // Use the index to match the track ID e.g. "[1]"
        const count = index + 1

        // Work out the delay for the track compared to the earliest source
        const startTime = (audioFile.start - delayDelta) * 1000

        /**
         * Generate the source arg and give it an audio track ID
         *
         * Note: delays are specified in stereo - delay-left-track|delay-right-track
         * e.g. [1]aresample=async=1,adelay=12.12|12.12[a1];
         */
        return `[${count}]aresample=async=1,adelay=${startTime}|${startTime}[a${count}];`
      }),

      // Tell ffmpeg to mix all these inputs into one with an ID of 'audio'
      `${mix}amix=inputs=${sources.length}[audio]`,
    ]

    // Start building the command's args list by forcing an overwrite
    const args = ['-y']

    // Loop all of our source files and specify them as inputs
    sources.forEach(({ audioFile }) => {
      args.push(`-i ${audioFile}`)
    })

    // Set up the filter with the resample config from above
    args.push(`-filter_complex "${resamples.join('')}"`)

    // Specify the map that ffmpeg should use instead of its default
    args.push('-map "[audio]"')

    // Specify the output codec for AWS
    args.push('-acodec libopus')

    // Tell ffmpeg where to output the file
    args.push(output)

    // Run the command
    return spawnProcess('ffmpeg', args, { shell: true })
  },

  getStartTime: async audioFile => {
    // Build some args
    const args = [
      // Specify a JSON output
      '-print_format', 'json',

      // Specify the info we want back
      '-show_entries',
      'format=start_time:format_tags=creation_time',

      // Feed in our file to inspect
      audioFile,
    ]
    const result = await spawnProcess('ffprobe', args)

    if (result) {
      try {
        // We should be able to parse the JSON and get our data out
        const { format: { start_time, tags } } = JSON.parse(result)

        // Return a structured object back
        return {
          audioFile,

          start: parseFloat(start_time),
          created: tags.creation_time,
        }
      } catch (err) {
        console.log('Could not parse JSON return', result)
      }
    }
    return null
  },
}
