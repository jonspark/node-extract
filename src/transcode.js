const {
  access,
  constants: fsConstants,
  createWriteStream,
  createReadStream,
  promises: {
    unlink,
  },
} = require('fs')
const { join } = require('path')

const { ElasticTranscoder, S3 } = require('aws-sdk')
const debug = require('debug')('ibo:handler:transcode')

const { uploadStream } = require('./s3')

const { combineAudioFiles, getStartTime } = require('./ffmpeg')

const FLAC_PRESET_ID = '1351620000001-300110'

const {
  AWS_ACCESS_KEY_ID,
  AWS_SECRET_ACCESS_KEY,
  AWS_TRANSCODE_PIPELINE: PIPELINE_ID,
  AWS_S3_BUCKET: BUCKET_NAME,
  TRANSCODE_CACHE_PATH = '/tmp',
} = process.env

const s3Client = new S3({
  credentials: {
    accessKeyId: AWS_ACCESS_KEY_ID,
    secretAccessKey: AWS_SECRET_ACCESS_KEY,
  },
})

exports.createJob = async (
  sourceFilePath,
  outputName = null,
  timeSpan = null,
) => {
  const transcoder = new ElasticTranscoder()

  // Break the path down into "folders"
  const keyParts = sourceFilePath.split('/')

  // Remove the filename and break it into name and extension
  const fileParts = outputName ? outputName.split('.') : keyParts.pop().split('.')

  // Remove the 'input' for output, join it back into a string path
  const outputFilePath = [...keyParts.slice(0, -2), 'output'].join('/')

  // Change the file extension
  const outputFileName = [...fileParts.slice(0, -1), 'flac'].join('.')

  const outputKey = `${outputFilePath}/${outputFileName}`

  const config = {
    PipelineId: PIPELINE_ID,
    Input: {
      Key: sourceFilePath,
    },
    Output: {
      Key: outputKey,
      PresetId: FLAC_PRESET_ID,
    },
  }

  if (timeSpan && timeSpan.startTime && timeSpan.duration) {
    config.Input.TimeSpan = {
      StartTime: timeSpan.startTime.toString(),
      Duration: timeSpan.duration.toString(),
    }
  }

  return transcoder.createJob(config).promise()
}

exports.createComposition = (s3Keys, outputFile) => new Promise(async (resolve, reject) => {
  try {
    debug('createComposition: Started.')
    const cleanUpPaths = []
    const basePath = join(__dirname, TRANSCODE_CACHE_PATH)

    debug(`createComposition: Downloading ${s3Keys.length} file(s)`)
    // First, fetch the audio from S3 and ffprobe for start times
    const files = await Promise.all(
      s3Keys.map(key => new Promise((resolve, reject) => {
        const filePath = join(basePath, key.split('/').pop())
        cleanUpPaths.push(filePath)

        // Check if the file exists in the cache to save a double-download
        access(filePath, fsConstants.F_OK, err => {
          if (!err) {
            debug('createComposition: File exists in cache, skipping download.')
            return resolve(filePath)
          }

          // Stream the file to a local cache from S3
          const file = createWriteStream(filePath)

          // Request the object with a promise
          debug(`createComposition: Downloading file: ${key}`)
          return s3Client.getObject({
            Bucket: BUCKET_NAME,
            Key: key,
          })
            .promise()
            .then(data => {
              file.write(data.Body, err => {
                // Close the stream
                file.end()

                // Resolve or reject as appropriate
                return err ? reject(err) : resolve(filePath)
              })
            })
            .catch(err => {
              console.log(err)
              reject(err)
            })
        })
      })),
    )

    debug('createComposition: Files downloaded, probing for start times')

    // ffprobe each local cache file for the relative start times
    const startTimes = await Promise.all(
      files.map(filePath => getStartTime(filePath)),
    )

    // Sort the array by ascending start times
    const orderedFiles = startTimes.sort((a, b) => a.start - b.start)

    // Create a temporary file path to output to
    const outputPath = join(basePath, outputFile)
    cleanUpPaths.push(outputPath)

    debug('createComposition: Start times found, starting combination')

    try {
      // Run the audio combination with ffmpeg
      await combineAudioFiles(orderedFiles, outputPath)

      debug('createComposition: Files combined, starting upload to S3')
      // Handle the upload of the composite file
      const uploadedObject = await new Promise(async (resolve, reject) => {
        // Make sure that the output file exists
        access(outputPath, fsConstants.F_OK, async err => {
          if (err) {
            reject(err)
          }

          try {
            /*
              * Create the upload key by:
              *
              * [1] Stripping the file name off the download key
              * [2] Combining it with the specified output file
              */
            const keyPrefix = s3Keys[0].split('/').slice(0, -1).join('/') // [1]
            const outputKey = `${keyPrefix}/${outputFile}` // [2]

            /**
             * Upload our combined file to S3
             *
             * [1] Create a stream to read our combined file
             * [2] Create an upload stream to S3
             * [3] Read one stream into the other
             * [4] Wait for the process to finish and return
             */
            // [1]
            const readStream = createReadStream(outputPath)

            // [2]
            const { promise, writeStream } = uploadStream(outputKey)

            // [3]
            readStream.pipe(writeStream)

            // [4]
            const {
              Bucket,
              Key,
              Location,
            } = await promise

            resolve({
              bucket: Bucket,
              key: Key,
              location: Location,
            })
          } catch (err) {
            reject(err)
          }
        })
      })

      debug('createComposition: Upload completed, cleaning up.')

      // Clean up temporary files
      await Promise.all(cleanUpPaths.map(
        cleanUpPath => {
          try {
            return unlink(cleanUpPath)
          } catch (err) {
            // a temporary file didn't clear
          }
          return null
        },
      ))

      debug('createComposition: Clean up complete.')

      return resolve(uploadedObject)
    } catch (err) {
      console.log('Problem combining audio: ', err)
      reject(err)
    }
    return null
  } catch (err) {
    console.log('Transcoder Error: ', err)
    reject(err)
  }
  return false
})

exports.getJob = jobId => new ElasticTranscoder().readJob({ Id: jobId }).promise()
